import com.google.common.collect.HashMultimap;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;

public class Main {

    static String inputFilePath = "./in.csv";
    static String outputFilePath = "./out.csv";
    private static final Object[] FILE_HEADER = {"DEMO-ID", "DEMO-URL", "BETA-URL", "mehrfach"};

    static List<Demo> demos = new ArrayList<Demo>();
    static HashMultimap<String, String> betas = HashMultimap.create();

    /**
     * Loads input file, parses the csv and writes an output file
     * @param args
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static void main(String[] args) throws FileNotFoundException, IOException {
        Iterable<CSVRecord> records = parseInputFile();
        parseLinesOfInputFile(records);
        writeOutputCSVData();

    }

    /**
     * 
     * @throws IOException
     */
    protected static void writeOutputCSVData() throws IOException {
        CSVFormat csvFileFormat = CSVFormat.DEFAULT.withRecordSeparator("\n").withDelimiter(';');
        CSVPrinter csvFilePrinter;
        try (FileWriter fileWriter = new FileWriter(outputFilePath)) {
            csvFilePrinter = new CSVPrinter(fileWriter, csvFileFormat);
            csvFilePrinter.printRecord(FILE_HEADER);

            findAndWriteData(csvFilePrinter);

            fileWriter.flush();
            csvFilePrinter.close();
        }
    }

    protected static void findAndWriteData(CSVPrinter csvFilePrinter) throws IOException {
        for (Demo demo : demos) {
            Set<String> betaUrls = betas.get(demo.id);
            if (betaUrls == null || betaUrls.isEmpty()) {
                csvFilePrinter.printRecord(demo.id, demo.url, "", "0");
            } else {
                for (String beta : betaUrls) {
                    csvFilePrinter.printRecord(demo.id, demo.url, beta, betaUrls.size());
                }
            }
        }
    }

    protected static void parseLinesOfInputFile(Iterable<CSVRecord> records) {
        for (CSVRecord record : records) {
            demos.add(new Demo(record.get("DEMO-ID"), record.get("DEMO-URL")));
            betas.put(record.get("BETA-ID"), record.get("BETA-URL"));
        }
    }

    protected static Iterable<CSVRecord> parseInputFile() throws FileNotFoundException, IOException {
        Reader in = new FileReader(inputFilePath);
        Iterable<CSVRecord> records = CSVFormat.EXCEL.withHeader().withDelimiter(';').parse(in);
        return records;
    }

    public static class Demo {

        public String id;
        public String url;

        public Demo(String id, String url) {
            this.id = id;
            this.url = url;
        }

    }
}