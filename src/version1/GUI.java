package version1;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * @author rogerwicki
 * GUI program for FileParser
 */
@SuppressWarnings("serial")
public class GUI extends JFrame implements ActionListener
{
	private JFrame mainFrame;
	
	private JLabel selectedFileLabel;
	private JLabel fieldDelimiterLabel;
	private JLabel descriptionLabel;
	
	private JButton selectFileButton;
	private JButton processButton;
	
	private JPanel fieldSet;
	private JPanel descriptionPanel;
	private JPanel commandButtonPanel;
	
	private JTextField fieldDelimiter;
	
	private String filePath;
	
	/**
	 * Constructor for GUI
	 */
	public GUI() {
		filePath = "No file selected";
		
		mainFrame = new JFrame("Matching Parser");
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainFrame.setSize(600, 250);
		mainFrame.setLayout(new BoxLayout(mainFrame.getContentPane(), BoxLayout.Y_AXIS));
		
		
		descriptionLabel = new JLabel();
		descriptionLabel.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
		descriptionLabel.setText("<html>This little Application allows selecting a file from your system and perform an URL matching. Use the button \"Browse\" below to select your file" +
				", optionally adapt the field delimiter and use the Button \"Process\" to start processing of your file.\n\nThe field delimiter describes, which character is used" +
				" to mark where the contents of two cells should be splitted horizontally. Usually, CSV data format use a comma (comma separated values), but MS Excel often uses " +
				"the semicolon instead.</html>");
		descriptionPanel = new JPanel(new BorderLayout());
		descriptionPanel.add(descriptionLabel, BorderLayout.NORTH);
		descriptionPanel.setPreferredSize(new Dimension(600,10));
		mainFrame.getContentPane().add(descriptionPanel);
		
		fieldSet = new JPanel(new FlowLayout(FlowLayout.LEFT));
		selectedFileLabel = new JLabel("<html>" + filePath + "</html>");
		selectFileButton = new JButton("Browse");
		selectFileButton.addActionListener(this);
		fieldDelimiterLabel = new JLabel("Delimiter");
		fieldDelimiter = new JTextField(",", 1);
		fieldSet.add(selectedFileLabel, BorderLayout.NORTH);
		fieldSet.add(selectFileButton, BorderLayout.AFTER_LAST_LINE);
		fieldSet.add(fieldDelimiterLabel, BorderLayout.AFTER_LAST_LINE);
		fieldSet.add(fieldDelimiter, BorderLayout.AFTER_LAST_LINE);
		fieldSet.setPreferredSize(new Dimension(600,60));
		mainFrame.getContentPane().add(fieldSet);
		
		commandButtonPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
		processButton = new JButton("Process");
		processButton.addActionListener(this);
		commandButtonPanel.add(processButton);
		mainFrame.getContentPane().add(commandButtonPanel);
	}
	
	/**
	 * @param args is an Object[] of arguments
	 */
	public static void main(String[] args) {
		GUI gui = new GUI();
		gui.showGUI();
    }
	
	/**
	 * Method to start showing the created GUI
	 */
	public void showGUI() {
		mainFrame.setVisible(true);
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		if ( e.getSource() == this.selectFileButton ) {
			JFileChooser chooser = new JFileChooser();
			chooser.setFileFilter(new FileNameExtensionFilter("Comma separated values (.csv)","csv"));
			int selection = chooser.showOpenDialog(null);
			if ( selection == JFileChooser.APPROVE_OPTION ) {
				filePath = chooser.getSelectedFile().getAbsolutePath();
				selectedFileLabel.setText("<html>" + filePath + "</html>");
			}
		} else if ( e.getSource() == this.processButton ) {
			FileParser fp = new FileParser(filePath, fieldDelimiter.getText());
			try {
				fp.execute();
			} catch (FileNotFoundException e1) {
				JOptionPane errorMessagePane = new JOptionPane();
				errorMessagePane.setOptions(new Object[] {JOptionPane.OK_OPTION});
				JOptionPane.showMessageDialog(errorMessagePane, "File not found");
				return;
			} catch (IOException e1) {
				JOptionPane errorMessagePane = new JOptionPane();
				errorMessagePane.setOptions(new Object[] {JOptionPane.OK_OPTION});
				JOptionPane.showMessageDialog(errorMessagePane, "Error");
				return;
			}
			JOptionPane successMessage = new JOptionPane();
			successMessage.setMessage("");
			successMessage.setOptions(new Object[] {JOptionPane.OK_OPTION});
			JOptionPane.showMessageDialog(successMessage, "Operation Successful");
		}
	}
}
