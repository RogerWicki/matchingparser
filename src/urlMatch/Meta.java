package urlMatch;

/**
 * @author rogerwicki
 *
 */
public class Meta extends DataSet
{
	protected String description;
	protected String length;
	protected String pixelWidth;
	
	/**
	 * @param compareUrl	the url match criteria
	 * @param description	the data set
	 * @param length		the description's length
	 * @param pixelWidth	the pixel width of description
	 * @param occurrences	the number of occurrences of this data set
	 */
	public Meta(String compareUrl, String occurrences, String description, String length, String pixelWidth) {
		super(compareUrl, occurrences);
		this.description = description;
		this.length = length;
		this.pixelWidth = pixelWidth;
	}

	/**
	 * Generates empty Meta
	 */
	public Meta() {
		super("","");
		this.description = "";
		this.length = "";
		this.pixelWidth = "";
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @return the length
	 */
	public String getLength() {
		return length;
	}

	/**
	 * @return the pixelWidth
	 */
	public String getPixelWidth() {
		return pixelWidth;
	}
}
