package urlMatch;

/**
 * @author rogerwicki
 */
public abstract class DataSet
{
	protected String compareUrl;
	protected String occurrences;
	
	/**
	 * @param compareUrl	the url match criteria
	 * @param occurrences	the number of occurrences of this data set
	 */
	public DataSet(String compareUrl, String occurrences) {
		this.compareUrl = compareUrl;
		this.occurrences = occurrences;
	}
	
	/**
	 * @return	the compareUrl as String
	 */
	public String getCompareUrl() {
		return this.compareUrl;
	}
	
	/**
	 * @return the occurrences
	 */
	public String getOccurrences() {
		return this.occurrences;
	}
}