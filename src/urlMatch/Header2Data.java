package urlMatch;

/**
 * @author rogerwicki
 *
 */
public class Header2Data extends DataSet
{
	protected String one;
	protected String length1;
	protected String two;
	protected String length2;

	/**
	 * @param compareUrl	the url match criteria
	 * @param occurrences	the number of occurrences of this data set
	 * @param one			data set one
	 * @param length1		one's length
	 * @param two			data set two
	 * @param length2		two's length
	 */
	public Header2Data(String compareUrl, String occurrences, String one, String length1, String two, String length2) {
		super(compareUrl, occurrences);
		this.one = one;
		this.length1 = length1;
		this.two = two;
		this.length2 = length2;
	}


	/**
	 * Generate empty Header2Data
	 */
	public Header2Data() {
		super("","");
		this.one = "";
		this.length1 = "";
		this.two = "";
		this.length2 = "";
	}
	/**
	 * @return the one
	 */
	public String getOne() {
		return one;
	}


	/**
	 * @return the length1
	 */
	public String getLength1() {
		return length1;
	}


	/**
	 * @return the two
	 */
	public String getTwo() {
		return two;
	}


	/**
	 * @return the length2
	 */
	public String getLength2() {
		return length2;
	}
}
