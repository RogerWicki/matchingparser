package urlMatch;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.util.HashMap;
import java.util.HashSet;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;

/**
 * @author de.nnis
 *
 */
public class FileParser
{
	private static String inputFilePath;
    private static String outputFilePath;
    private static char delimiter;
    private final static Object[] FILE_OUTPUT_HEADER = {"COMPARE_BETA_URL", "DEMO_OCCURRENCES", "DEMO_ID", "DEMO_URL", "H1_OCCURRENCE", "H1_1", "H1_1_LENGTH", "H2_OCCURRENCES", "H2_1", "H2_1_LENGTH", "H2_2", "H2_2_LENGTH", "META_OCCURRENCES", "META_DESCRIPTION", "META_LENGTH", "META_PIXEL_WIDTH", "TITLE_OCCURRENCES", "TITLE_1", "TITLE_1_LENGTH"};
    private static HashMap<String, HashMap<String,DataSet>> dataSets;
    private static HashSet<String> compareUrls;
	
	/**
	 * @param filePath String pointing out the file path to load file from and to write file to
	 * @param delimiter one character long String indicating which delimiter should be used
	 */
	public FileParser(String filePath, String delimiter) {
		FileParser.inputFilePath = filePath;
		FileParser.outputFilePath = filePath.replace(".csv", "_processed.csv");
		FileParser.delimiter = delimiter.charAt(0);
		dataSets = new HashMap<String, HashMap<String, DataSet>>();
		compareUrls = new HashSet<String>();
	}
	
	/**
	 * Loads and then parses the file in the provided input location, then writes an output file in the same location
	 * @throws FileNotFoundException if the file could not be found at provided location (i.e. someone deleted it after selection), this error is thrown
	 * @throws IOException if the file could not be loaded
	 */
	public void execute() throws FileNotFoundException, IOException {
		Iterable<CSVRecord> records = parseInputFile(inputFilePath, delimiter);
		parseLinesOfInputFile(records);
		writeOutputCSVData();
	}
	
    /**
     * Creates and writes output data
     * @throws IOException
     */
    protected static void writeOutputCSVData() throws IOException {
        CSVFormat csvFileFormat = CSVFormat.DEFAULT.withRecordSeparator("\n").withDelimiter(delimiter);
        CSVPrinter csvFilePrinter;
        try (FileWriter fileWriter = new FileWriter(outputFilePath)) {
            csvFilePrinter = new CSVPrinter(fileWriter, csvFileFormat);
            csvFilePrinter.printRecord(FILE_OUTPUT_HEADER);

            findAndWriteData(csvFilePrinter);

            fileWriter.flush();
            csvFilePrinter.close();
        }
    }
    
    /**
     * Checks how many occurrences of a demo Id is found in the HashMultimap of betas and maps them together.
     * @param csvFilePrinter is a CSVPrinter
     * @throws IOException
     */
    protected static void findAndWriteData(CSVPrinter csvFilePrinter) throws IOException {
    	for (String compareUrl : compareUrls) {
    		DemoData demo = (DemoData) (dataSets.containsKey("DEMODATA") ? (dataSets.get("DEMODATA").containsKey(compareUrl) ? dataSets.get("DEMODATA").get(compareUrl) : new DemoData()) : new DemoData());
    		Header1Data header1 = (Header1Data) (dataSets.containsKey("HEADER1") ? (dataSets.get("HEADER1").containsKey(compareUrl) ? dataSets.get("HEADER1").get(compareUrl) : new Header1Data()) : new Header1Data());
    		Header2Data header2 = (Header2Data) (dataSets.containsKey("HEADER2") ? (dataSets.get("HEADER2").containsKey(compareUrl) ? dataSets.get("HEADER2").get(compareUrl) : new Header2Data()) : new Header2Data());
    		Meta meta = (Meta) (dataSets.containsKey("META") ? (dataSets.get("META").containsKey(compareUrl) ? dataSets.get("META").get(compareUrl) : new Meta()) : new Meta());
    		Title title = (Title) (dataSets.containsKey("TITLE") ? (dataSets.get("TITLE").containsKey(compareUrl) ? dataSets.get("TITLE").get(compareUrl) : new Title()) : new Title());
    		csvFilePrinter.printRecord(compareUrl, demo.getOccurrences(), demo.getDemoId(), demo.getDemoUrl(), header1.getOccurrences(), header1.getOne(), header1.getLength1(), header2.getOccurrences(), header2.getOne(), header2.getLength1(), header2.getTwo(), header2.getLength2(), meta.getOccurrences(), meta.getDescription(), meta.getLength(), meta.getPixelWidth(), title.getOccurrences(), title.getOne(), title.getLength1());
    	}
    }
    
    /**
     * Creates a demo object with Demo ID and Demo URL and maps Beta Id to Beta URL in a HashMultimap
     * @param records is an Iterable<CSVRecord>
     * @throws IllegalArgumentException
     * @throws IllegalStateException
     */
    protected static void parseLinesOfInputFile(Iterable<CSVRecord> records) throws IllegalArgumentException, IllegalStateException {
        for (CSVRecord record : records) {
            compareUrls.add(record.get("BETA_URL"));
            
            DataSet demoData = new DemoData(record.get("BETA_URL"), record.get("BETA_MULTIPLE"), record.get("DEMO_ID"), record.get("DEMO_URL"));
            if (!dataSets.containsKey("DEMODATA")) {
            	dataSets.put("DEMODATA", new HashMap<String, DataSet>());
            }
            dataSets.get("DEMODATA").put(demoData.getCompareUrl(), demoData);
            
            DataSet header1 = new Header1Data(record.get("H1_URL"), record.get("H1_OCCURRENCES"), record.get("H1_1"), record.get("H1_1_LENGTH"));
            if (!dataSets.containsKey("HEADER1")) {
            	dataSets.put("HEADER1", new HashMap<String, DataSet>());
            }
            dataSets.get("HEADER1").put(demoData.getCompareUrl(), header1);
            
            DataSet header2 = new Header2Data(record.get("H2_URL"), record.get("H2_OCCURRENCES"), record.get("H2_1"), record.get("H2_1_LENGTH"), record.get("H2_2"), record.get("H2_2_LENGTH"));
            if (!dataSets.containsKey("HEADER2")) {
            	dataSets.put("HEADER2", new HashMap<String, DataSet>());
            }
            dataSets.get("HEADER2").put(demoData.getCompareUrl(), header2);
            
            DataSet meta = new Meta(record.get("META_URL"), record.get("META_OCCURRENCES"), record.get("META_DESCRIPTION"), record.get("META_LENGTH"), record.get("META_PIXEL_WIDTH"));
            if (!dataSets.containsKey("META")) {
            	dataSets.put("META", new HashMap<String, DataSet>());
            }
            dataSets.get("META").put(demoData.getCompareUrl(), meta);
            
            DataSet title = new Title(record.get("TITLE_URL"), record.get("TITLE_OCCURRENCES"), record.get("TITLE_1"), record.get("TITLE_1_LENGTH"));
            if (!dataSets.containsKey("TITLE")) {
            	dataSets.put("TITLE", new HashMap<String, DataSet>());
            }
            dataSets.get("TITLE").put(demoData.getCompareUrl(), title);
        }
    }
    /**
     * Loads the file and returns its content as an Iterable<CSVRecord>
     * @param inputFilePath is the absolute path to the input file
     * @param delimiter is a char for as delimiter of file
     * @return an Iterable<CSVRecord> out of file at provided location with provided delimiter
     * @throws FileNotFoundException
     * @throws IOException
     */
    protected static Iterable<CSVRecord> parseInputFile(String inputFilePath, char delimiter) throws FileNotFoundException, IOException {
        Reader in = new FileReader(inputFilePath);
        Iterable<CSVRecord> records = CSVFormat.EXCEL.withHeader().withDelimiter(delimiter).parse(in);
        return records;
    }

}
