package urlMatch;

/**
 * @author rogerwicki
 *
 */
public class DemoData extends DataSet
{
	protected String demoId;
	protected String demoUrl;

	/**
	 * @param compareUrl	the url match criteria
	 * @param occurrences	the number of occurrences of this data set
	 * @param demoId		a demo id
	 * @param demoUrl		a demo url
	 */
	public DemoData(String compareUrl, String occurrences, String demoId, String demoUrl) {
		super(compareUrl, occurrences);
		this.demoId = demoId;
		this.demoUrl = demoUrl;
	}

	/**
	 * Generates empty DemoData
	 */
	public DemoData() {
		super("","");
		this.demoId = "";
		this.demoUrl = "";
	}

	/**
	 * @return the demoId
	 */
	public String getDemoId() {
		return demoId;
	}

	/**
	 * @return the demoUrl
	 */
	public String getDemoUrl() {
		return demoUrl;
	}
}
