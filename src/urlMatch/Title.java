package urlMatch;

/**
 * @author rogerwicki
 *
 */
public class Title extends DataSet
{
	protected String one;
	protected String length1;
	
	/**
	 * @param compareUrl	the url match criteria
	 * @param occurrences	the number of occurrences of this data set
	 * @param one			data set one
	 * @param length1		one's length
	 */
	public Title(String compareUrl, String occurrences, String one, String length1) {
		super(compareUrl, occurrences);
		this.one = one;
		this.length1 = length1;
	}

	/**
	 * Generates empty Title
	 */
	public Title() {
		super("","");
		this.one = "";
		this.length1 = "";
	}

	/**
	 * @return the one
	 */
	public String getOne() {
		return one;
	}

	/**
	 * @return the length1
	 */
	public String getLength1() {
		return length1;
	}
}
