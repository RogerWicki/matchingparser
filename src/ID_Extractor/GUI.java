package ID_Extractor;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;

public class GUI implements ActionListener {

private JFrame mainFrame;
	
	private JLabel selectedFileLabel;
	private JLabel fieldLabel;
	private JLabel descriptionLabel;
	
	private JButton selectFileButton;
	private JButton processButton;
	
	private JPanel fieldSet;
	private JPanel descriptionPanel;
	private JPanel commandButtonPanel;
	
	private JTextField fieldDelimiter;
	private JTextField fieldId;
	
	private JTextArea statusMessage;
	
	private String filePath;
	
	/**
	 * Constructor for GUI
	 */
	public GUI() {
		filePath = "No file selected";
		
		mainFrame = new JFrame("Matching Parser");
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainFrame.setSize(600, 250);
		mainFrame.setLayout(new BoxLayout(mainFrame.getContentPane(), BoxLayout.Y_AXIS));
		
		
		descriptionLabel = new JLabel();
		descriptionLabel.setText("<html><p style=\"width:400px;\">This little Application allows selecting a csv file from your system and export a partial file according to a "
				+ "given criteria.</p></html>");
		descriptionPanel = new JPanel();
		descriptionPanel.add(descriptionLabel);
		mainFrame.getContentPane().add(descriptionPanel);
		
		fieldSet = new JPanel();
		fieldSet.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
		selectedFileLabel = new JLabel("<html>" + filePath + "</html>");
		selectFileButton = new JButton("Browse");
		selectFileButton.addActionListener(this);
		fieldLabel = new JLabel("Delimiter");
		fieldDelimiter = new JTextField(",", 1);
		fieldSet.add(selectedFileLabel);
		fieldSet.add(selectFileButton);
		fieldSet.add(fieldLabel);
		fieldSet.add(fieldDelimiter);
		fieldLabel = new JLabel("ID Start");
		fieldId = new JTextField("",7);
		fieldSet.add(fieldLabel);
		fieldSet.add(fieldId);
		fieldSet.setPreferredSize(new Dimension(600,80));
		mainFrame.getContentPane().add(fieldSet);
		statusMessage = new JTextArea("");
		statusMessage.setEditable(false);
		statusMessage.setVisible(false);
		mainFrame.getContentPane().add(statusMessage);
		
		commandButtonPanel = new JPanel();
		processButton = new JButton("Process");
		processButton.addActionListener(this);
		commandButtonPanel.add(processButton);
		mainFrame.getContentPane().add(commandButtonPanel);
	}
	
	/**
	 * @param args is an Object[] of arguments
	 */
	public static void main(String[] args) {
		GUI gui = new GUI();
		gui.showGUI();
    }
	
	/**
	 * Method to start showing the created GUI
	 */
	public void showGUI() {
		mainFrame.setVisible(true);
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		if ( e.getSource() == this.selectFileButton ) {
			JFileChooser chooser = new JFileChooser();
			chooser.setFileFilter(new FileNameExtensionFilter("Comma separated values (.csv)","csv"));
			int selection = chooser.showOpenDialog(null);
			if ( selection == JFileChooser.APPROVE_OPTION ) {
				filePath = chooser.getSelectedFile().getAbsolutePath();
				selectedFileLabel.setText("<html>" + filePath + "</html>");
			}
		} else if ( e.getSource() == this.processButton ) {
			try {
				IDExtractor.validateInput(fieldId.getText());
			} catch(IDExtractor.InvalidInputException iie) {
				statusMessage.setText("<html>" + iie.getMessage() + "</html>");
				statusMessage.setVisible(true);
				return;
			}
			IDExtractor extractor = new IDExtractor(fieldId.getText(), filePath, fieldDelimiter.getText());
			try {
				extractor.execute();
			} catch (FileNotFoundException fnfe) {
				System.out.println("There was an error:\n" + fnfe.getCause() + "\n" + fnfe.getStackTrace().toString());
			} catch (IOException io) {
				System.out.println("There was an error:\n" + io.getCause() + "\n" + io.getStackTrace().toString());
			}
		}
	}

}
