package ID_Extractor;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.util.HashMap;
import java.util.regex.Pattern;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;

public class IDExtractor {
	@SuppressWarnings("serial")
	public static class InvalidInputException extends Exception{
		public InvalidInputException(String s) {
			super(s);
		}
	}
	
	private final static Object[] FILE_OUTPUT_HEADER = {"ID", "Page Views"};
	private String inputFilePath;
	private static String outputFilePath;
	private static char delimiter;
	private static String input;
	private static HashMap<String, String> outputData;
	
	public IDExtractor(String input, String filePath, String delimiter) {
		IDExtractor.input = input;
		IDExtractor.delimiter = delimiter.charAt(0);
		IDExtractor.outputFilePath = filePath.replace(".csv", "_processed.csv");
		this.inputFilePath = filePath;
		outputData = new HashMap<String, String>();
	}
	
	public static void validateInput(String input) throws InvalidInputException {
		if (!Pattern.compile("([1-9]{1})([0-9]{0,6})").matcher(input).matches()) {
			throw new IDExtractor.InvalidInputException("The input does not meet the required conditions!");
		}
	}
	
	public void execute() throws FileNotFoundException, IOException {
		parseLinesOfInputFile(parseInputFile(inputFilePath, delimiter));
		writeOutputCSVData();
	}
	
	/**
	 * Loads the file and returns its content as an Iterable<CSVRecord>
	 * @param inputFilePath is the absolute path to the input file
	 * @param delimiter is a char for as delimiter of file
	 * @return an Iterable<CSVRecord> out of file at provided location with provided delimiter
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	protected static Iterable<CSVRecord> parseInputFile(String inputFilePath, char delimiter) throws FileNotFoundException, IOException {
		Reader in = new FileReader(inputFilePath);
		Iterable<CSVRecord> records = CSVFormat.DEFAULT.withHeader().withDelimiter(delimiter).parse(in);
		return records;
	}
	
	protected void parseLinesOfInputFile(Iterable<CSVRecord> records) throws IllegalArgumentException, IllegalStateException {
		Pattern inputPattern = Pattern.compile(".*\\/" + input + "([0-9]{" + (7 - input.length()) + "}).*");
        for (CSVRecord record : records) {
        	if ( inputPattern.matcher(record.get("Seite")).matches() ) {
        		String linkId = getLinkId(record.get("Seite"), inputPattern.toString());
        		if ( outputData.containsKey(linkId) ) {
        			outputData.put(linkId, String.valueOf(Integer.parseInt(outputData.get(linkId)) + Integer.parseInt(record.get("Seitenaufrufe"))));
        		} else {
        			outputData.put(linkId, record.get("Seitenaufrufe"));
        		}
        	}
        }
	}
	
	/**
	 * Extracts the link ID out of given link
	 * @param link is a full link from the architonic website
	 * @param regex is the pattern based on the user's input to find specific IDs
	 * @return The link ID as String or an empty String if there was no match with the pattern
	 */
	protected static String getLinkId(String link, String regex) {
		String[] removeParts = link.split(regex.replace(".*", ""));
		for ( int i = 0; i < removeParts.length; i++ ) {
			link = link.replace(removeParts[i], "");
		}
		return link.replaceAll("/", "");
	}
	
	/**
	 * Creates and writes output data
	 * @throws IOException
	 */
	protected static void writeOutputCSVData() throws IOException {
		CSVFormat csvFileFormat = CSVFormat.DEFAULT.withRecordSeparator("\n").withDelimiter(delimiter);
		CSVPrinter csvFilePrinter;
		try (FileWriter fileWriter = new FileWriter(outputFilePath)) {
			csvFilePrinter = new CSVPrinter(fileWriter, csvFileFormat);
			csvFilePrinter.printRecord(FILE_OUTPUT_HEADER);

			for ( String id : outputData.keySet() ) {
				csvFilePrinter.printRecord(id, outputData.get(id));
			}

			fileWriter.flush();
			csvFilePrinter.close();
		}
	}	
}
