package version2;

import com.google.common.collect.HashMultimap;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;

/**
 * @author de.nnis
 *
 */
public class FileParser
{
	private static String inputFilePath;
    private static String outputFilePath;
    private static char delimiter;
    private final static Object[] FILE_HEADER = {"DEMO-ID", "DEMO-URL", "BETA-URL", "mehrfach"};
    private static List<Demo> demos = new ArrayList<Demo>();
    private static HashMultimap<String, String> betas = HashMultimap.create();
	
	/**
	 * @param filePath String pointing out the file path to load file from and to write file to
	 * @param delimiter one character long String indicating which delimiter should be used
	 */
	public FileParser(String filePath, String delimiter) {
		FileParser.inputFilePath = filePath;
		FileParser.outputFilePath = filePath.replace(".csv", "_processed.csv");
		FileParser.delimiter = delimiter.charAt(0);
	}
	
	/**
	 * Loads and then parses the file in the provided input location, then writes an output file in the same location
	 * @throws FileNotFoundException if the file could not be found at provided location (i.e. someone deleted it after selection), this error is thrown
	 * @throws IOException if the file could not be loaded
	 */
	public void execute() throws FileNotFoundException, IOException {
		Iterable<CSVRecord> records = parseInputFile(inputFilePath, delimiter);
		parseLinesOfInputFile(records);
		writeOutputCSVData();
	}
	
    /**
     * Creates and writes output data
     * @throws IOException
     */
    protected static void writeOutputCSVData() throws IOException {
        CSVFormat csvFileFormat = CSVFormat.DEFAULT.withRecordSeparator("\n").withDelimiter(delimiter);
        CSVPrinter csvFilePrinter;
        try (FileWriter fileWriter = new FileWriter(outputFilePath)) {
            csvFilePrinter = new CSVPrinter(fileWriter, csvFileFormat);
            csvFilePrinter.printRecord(FILE_HEADER);

            findAndWriteData(csvFilePrinter);

            fileWriter.flush();
            csvFilePrinter.close();
        }
    }
    
    /**
     * Checks how many occurrences of a demo Id is found in the HashMultimap of betas and maps them together.
     * @param csvFilePrinter is a CSVPrinter
     * @throws IOException
     */
    protected static void findAndWriteData(CSVPrinter csvFilePrinter) throws IOException {
        for (Demo demo : demos) {
            Set<String> betaUrls = betas.get(demo.id);
            if (betaUrls == null || betaUrls.isEmpty()) {
                csvFilePrinter.printRecord(demo.id, demo.url, "", "0");
            } else {
                for (String beta : betaUrls) {
                    csvFilePrinter.printRecord(demo.id, demo.url, beta, betaUrls.size());
                }
            }
        }
    }
    
    /**
     * Creates a demo object with Demo ID and Demo URL and maps Beta Id to Beta URL in a HashMultimap
     * @param records is an Iterable<CSVRecord>
     * @throws IllegalArgumentException
     * @throws IllegalStateException
     */
    protected static void parseLinesOfInputFile(Iterable<CSVRecord> records) throws IllegalArgumentException, IllegalStateException {
        for (CSVRecord record : records) {
            demos.add(new Demo(record.get("DEMO-ID"), record.get("DEMO-URL")));
            betas.put(record.get("BETA-ID"), record.get("BETA-URL"));
        }
    }

    /**
     * Loads the file and returns its content as an Iterable<CSVRecord>
     * @param inputFilePath is the absolute path to the input file
     * @param delimiter is a char for as delimiter of file
     * @return an Iterable<CSVRecord> out of file at provided location with provided delimiter
     * @throws FileNotFoundException
     * @throws IOException
     */
    protected static Iterable<CSVRecord> parseInputFile(String inputFilePath, char delimiter) throws FileNotFoundException, IOException {
        Reader in = new FileReader(inputFilePath);
        Iterable<CSVRecord> records = CSVFormat.EXCEL.withHeader().withDelimiter(delimiter).parse(in);
        return records;
    }

	/**
	 * @author de.nnis
	 * Class to save Demo data
	 */
	public static class Demo {

        private String id;
        private String url;

        /**
         * @param id String representing the URL Id
         * @param url String containing the whole URL
         */
        public Demo(String id, String url) {
            this.id = id;
            this.url = url;
        }

    }
}
